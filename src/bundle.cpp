#include "copper.hpp"

using namespace Copper;

string make_path_safe(const string& path)
{
	if(path.empty()) throw invalid_argument("Empty bundle path!");

	if(path.back() == '/') {
		return path;
	} else {
		return path + '/';
	}
}

void trim(string& str)
{
	while(!str.empty()) {
		if(str.front() != ' ' && str.front() != '\t') break;
		str.erase(0, 1);
	}

	while(!str.empty()) {
		if(str.back() != ' ' && str.back() != '\t') break;
		str.pop_back();
	}
}

Bundle::Bundle(HTTPServer& server, const string& path)
	: server{&server}, path{make_path_safe(path)}
{
	load();
	enable();
}

void Bundle::read_infos()
{
	ifstream stream(path + "bundle");

	if(!stream.is_open()) {
		throw invalid_argument("Cannot open informations file!");
	}

	string buffer;
	unsigned int line = 0;

	while(getline(stream, buffer)) {
		line++;

		trim(buffer);
		if(buffer.empty()) continue;

		const auto i = buffer.find('=');

		if(i == string::npos) {
			throw invalid_argument(string("Invalid information file! (Line ")
				+ to_string(line) + ")");
		}

		string name(buffer.cbegin(), buffer.cbegin() + i);
		trim(name);

		string value(buffer.cbegin() + i + 1, buffer.cend());
		trim(value);

		if(info_fields.find(name) != info_fields.cend()) {
			throw invalid_argument(string("Duplicate information field `")
				+ name + "`!");
		}

		info_fields.emplace(name, value);
	}

	if(!has_field("name")
		|| !has_field("description")
		|| !has_field("version")) {
		throw invalid_argument("Missing mandatory information field!");
	}
}

void Bundle::read_resources()
{
	ifstream stream(path + "resources");
	if(!stream.is_open()) return;

	string buffer;

	while(getline(stream, buffer)) {
		trim(buffer);
		if(buffer.empty()) continue;

		string host = "*";
		size_t pos;

		if((pos = buffer.find(':')) != string::npos) {
			host = string(buffer.cbegin(), buffer.cbegin() + pos);
			buffer = string(buffer.cbegin() + pos + 1, buffer.cend());
		}

		register_handler<ResourceHandler>(host, path, buffer);
	}
}

void Bundle::load()
{
	const auto p = path + BUNDLE_BINARY;
	bundle = dlopen(p.c_str(), RTLD_LAZY);

	if(!bundle) {
		throw invalid_argument(dlerror());
	}

	dlerror();
}

void Bundle::enable()
{
	read_infos();
	read_resources();

	void (*enable)(Bundle&);
	*(void **) (&enable) = dlsym(bundle, "enable");

	if(dlerror()) {
		throw invalid_argument("Failed to enable bundle");
	}

	(*enable)(*this);

	enabled = true;
}

void Bundle::disable()
{
	void (*disable)();
	*(void **) (&disable) = dlsym(bundle, "disable");

	if(dlerror()) {
		throw invalid_argument("Failed to disable bundle");
	}

	(*disable)();

	info_fields.clear();

	handlers.clear();
	override_handlers.clear();

	templates.clear();

	commands.clear();

	enabled = false;
}

void Bundle::reload()
{
	if(enabled) disable();
	enable();
}

Handler* Bundle::get_handler(const Call& call) const
{
	for(const auto& h : handlers) {
		if(h->can_handle(call)) {
			return h.get();
		}
	}

	return nullptr;
}

Handler* Bundle::get_handler(const http_code_t code) const
{
	const auto handler = override_handlers.find(code);
	return (handler != override_handlers.cend()
		? handler->second.get() : nullptr);
}

const string& Bundle::get_template(const string& name)
{
	const auto t = templates.find(name);

	if(t == templates.cend()) {
		ifstream stream(get_path() + "templates/" + name);

		if(!stream.is_open()) {
			throw invalid_argument("Can't find template `"s + name + "`!");
		}

		stream.seekg(0, ios::end);
		const auto size = stream.tellg();

		string buffer;
		buffer.resize(size);

		stream.seekg(0);
		stream.read(&buffer[0], size);

		return (templates[name] = buffer);
	} else {
		return t->second;
	}
}
