#include "command.hpp"
#include "../copper.hpp"

using namespace Copper;

void BundlesCommand::perform(HTTPServer& server, const vector<string>&) const
{
	cout << "Loaded bundles:" << '\n';
	cout << '\n';

	for(const auto& b : server.get_bundles()) {
		const auto& fields = b.get_fields();
		cout << "- " << b.get_path() << ": " << fields.at("name")
			<< " (" << (b.is_enabled() ? "Enabled" : "Disabled")
			<< ", Version: " << fields.at("version") << "): "
			<< fields.at("description") << '\n';
	}
}
