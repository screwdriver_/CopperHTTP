#include "command.hpp"
#include "../copper.hpp"

using namespace Copper;

void ExitCommand::perform(HTTPServer& server, const vector<string>&) const
{
	server.log("Stopping listeners...");

	server.stop_http();
	server.stop_https();

	server.log("Disabling bundles...");

	for(auto& b : server.get_bundles()) {
		b.disable();
	}

	exit(0);
}
