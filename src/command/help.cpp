#include "command.hpp"
#include "../copper.hpp"

using namespace Copper;

void HelpCommand::perform(HTTPServer& server, const vector<string>&) const
{
	cout << "CopperHTTP version " << VERSION << '\n';
	cout << '\n';

	for(const auto& c : server.get_commands()) {
		cout << c->get_usage() << "\t\t" << c->get_description() << '\n';
	}
}
