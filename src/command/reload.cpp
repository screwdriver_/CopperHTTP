#include "command.hpp"
#include "../copper.hpp"

using namespace Copper;

void ReloadCommand::perform(HTTPServer& server, const vector<string>&) const
{
	for(auto& b : server.get_bundles()) {
		const auto& name = b.get_fields().find("name")->second;

		cout << "Reloading bundle `" << name << "`...\n";
		b.reload();
	}

	cout << "Done.\n";
}
