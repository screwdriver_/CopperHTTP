#ifndef COPPER_HPP
# define COPPER_HPP

# include <array>
# include <cctype>
# include <chrono>
# include <fstream>
# include <iostream>
# include <memory>
# include <mutex>
# include <thread>
# include <sstream>
# include <string>
# include <unordered_map>
# include <vector>

# include <dirent.h>
# include <dlfcn.h>
# include <errno.h>
# include <stdio.h>
# include <sys/stat.h>
# include <sys/types.h>

# include <socket.hpp>

# include "command/command.hpp"
# include "handler/handler.hpp"

# define VERSION			"0.1"

# define TMP_DIRECTORY		"tmp/"
# define BUNDLES_DIRECTORY	"bundles/"
# define BUNDLE_BINARY		"bundle.so"

# define HTTP_DEFAULT		80
# define HTTPS_DEFAULT		443

# define CALL_TIMEOUT		30

namespace Copper
{
	using namespace std;
	using namespace Network;

	typedef uint16_t port_t;
	typedef uint16_t http_code_t;

	class HTTPServer;
	class Call;

	/**
	 * The list of all http response codes.
	 */
	enum http_code : http_code_t
	{
		CONTINUE = 100,
		SWITCHING_PROTOCOLS = 101,
		PROCESSING = 102,

		OK = 200,
		CREATED = 201,
		ACCEPTED = 202,
		NON_AUTHORITATIVE_INFORMATION = 203,
		NO_CONTENT = 204,
		RESET_CONTENT = 205,
		PARTIAL_CONTENT = 206,
		MULTI_STATUS = 207,
		ALREADY_REPORTED = 208,
		IM_USED = 226,

		MULTIPLE_CHOICES = 300,
		MOVED_PERMANENTELY = 301,
		FOUND = 302,
		SEE_OTHER = 303,
		NOT_MODIFIED = 304,
		USE_PROXY = 305,
		SWITCH_PROXY = 306,
		TEMPORARY_REDIRECT = 307,
		PERMANENT_REDIRECT = 308,

		BAD_REQUEST = 400,
		UNAUTHORIZED = 401,
		PAYMENT_REQUIRED = 402,
		FORBIDDEN = 403,
		NOT_FOUND = 404,
		METHOD_NOT_ALLOWED = 405,
		NOT_ACCEPTABLE = 406,
		PROXY_AUTHENTICATION_REQUIRED = 407,
		REQUEST_TIMEOUT = 408,
		CONFLICT = 409,
		GONE = 410,
		LENGTH_REQUIRED = 411,
		PRECONDITION_FAILED = 412,
		PAYLOAD_TOO_LARGE = 413,
		URI_TOO_LONG = 414,
		UNSUPPORTED_MEDIA_TYPE = 415,
		RANGE_NOT_SATISFIABLE = 416,
		EXPECTATION_FAILED = 417,
		IM_A_TEAPOT = 418,
		MISDIRECTED_REQUEST = 421,
		UNPROCESSABLE_ENTITY = 422,
		LOCKED = 423,
		FAILED_DEPENDENCY = 424,
		UPGRADE_REQUIRED = 426,
		PRECONDITION_REQUIRED = 428,
		TOO_MANY_REQUESTS = 429,
		REQUEST_HEADER_FIELDS_TOO_LARGE = 431,
		UNAVAILABLE_FOR_LEGAL_REASONS = 451,

		INTERNAL_SERVER_ERROR = 500,
		NOT_IMPLEMENTED = 501,
		BAD_GATEWAY = 502,
		SERVICE_UNAVAILABLE = 503,
		GATEWAY_TIMEOUT = 504,
		HTTP_VERSION_NOT_SUPPORTED = 505,
		VARIANT_ALSO_NEGOTIATES = 506,
		INSUFFICIENT_STORAGE = 507,
		LOOP_DETECTED = 508,
		NOT_EXTENDED = 510,
		NETWORK_AUTHENTICATION_REQUIRED = 511
	};

	enum http_method : uint8_t
	{
		GET = 0,
		HEAD = 1,
		POST = 2,
		PUT = 3,
		DELETE = 4,
		TRACE = 5,
		OPTIONS = 6,
		CONNECT = 7,
		PATCH = 8,

		UNKNOWN = 9
	};

	inline bool str_startsWith(const string& haystack, const string& needle)
	{
		return (haystack.find(needle) == 0);
	}

	/**
	 * The class representing bundles. A bundle is a shared library which is loaded by the webserver and allow to register commands, handlers or to add new features to the server.
	 * Bundles are commonly stored into the bundles/ directory, but they can be loaded from anywhere.
	 *
	 * A bundle's directory must contain at least the following files:
	 * - bundle.so: The shared library
	 * - bundle: An information file
	 */
	class Bundle
	{
		public:
			/**
			 * Loads and enable a new bundle.
			 *
			 * \param server	The server the bundle is associated to
			 * \param path		The path to the bundle's directory
			 */
			Bundle(HTTPServer& server, const string& path);
			Bundle(Bundle&&) = default;

			/**
			 * \return	The server instance the bundle is associated to.
			 */
			inline HTTPServer& get_server()
			{
				return *server;
			}

			/**
			 * \return	The server instance the bundle is associated to.
			 */
			inline const HTTPServer& get_server() const
			{
				return *server;
			}

			/**
			 * \return	The path to the bundle's directory.
			 */
			inline const string get_path() const
			{
				return path;
			}

			/**
			 * Tells whether the bundle's information file contains the specified field or not.
			 *
			 * \param name	The field's name
			 */
			inline bool has_field(const string& name) const
			{
				return (info_fields.find(name) != info_fields.cend());
			}

			/**
			 * Tells whether the bundle's information file contains the specified field or not.
			 *
			 * \param name	The field's name
			 */
			inline bool has_field(const string&& name) const
			{
				return has_field(name);
			}

			/**
			 * \return The list of all the information file's fields.
			 */
			inline const unordered_map<string, string>& get_fields() const
			{
				return info_fields;
			}

			/**
			 * Tells whether the bundle is enabled or not.
			 */
			inline bool is_enabled() const
			{
				return enabled;
			}

			/**
			 * Enables the bundle.
			 */
			void enable();

			/**
			 * Disables the bundle (This will unregister all handlers and commands).
			 */
			void disable();

			/**
			 * Reloads the bundle.
			 */
			void reload();

			/**
			 * Registers a handler for this bundle.
			 *
			 * \tparam H	The handler's class
			 * \tparam Args	Arguments to construct the handler's object
			 *
			 * \param args	Arguments to construct the handler's object
			 */
			template<typename H, typename... Args>
			inline void register_handler(Args&&... args)
			{
				handlers.emplace_back(new H(args...));
			}

			/**
			 * Registers an override handler for this bundle.
			 * Override handlers are associated to an http response code.
			 * They are called when the default response code retrieved before handler lookup is the one associated to the said override handler.
			 *
			 * \tparam H	The handler's class
			 * \tparam Args	Arguments to construct the handler's object
			 *
			 * \param code	The handler's associated http response code
			 * \param args	Arguments to construct the handler's object
			 */
			template<typename H, typename... Args>
			inline void register_override_handler(const http_code_t code,
				Args&&... args)
			{
				override_handlers.emplace(code, unique_ptr<H>(new H(args...)));
			}

			/**
			 * \return A list of all registered handlers
			 */
			inline const vector<unique_ptr<Handler>>& get_handlers() const
			{
				return handlers;
			}

			/**
			 * \return A list of all registered override handlers
			 */
			inline const unordered_map<http_code_t,
				unique_ptr<Handler>>& get_override_handlers() const
			{
				return override_handlers;
			}

			/**
			 * Returns a handler for the specified call.
			 *
			 * \param call	The call
			 */
			Handler* get_handler(const Call& call) const;

			/**
			 * Returns an override handler for the specified http response code.
			 *
			 * \param code	The http response code
			 */
			Handler* get_handler(const http_code_t code) const;

			const string& get_template(const string& name);

			inline const string& get_template(const string&& name)
			{
				return get_template(name);
			}

			/**
			 * Registers a command.
			 *
			 * \param C		The command's class
			 * \param Args	Arguments to construct the command's object
			 */
			template<typename C, typename... Args>
			inline void register_command(Args&&... args)
			{
				commands.emplace_back(new C(args...));
			}

			/**
			 * \return A list of all registered commands
			 */
			inline const vector<unique_ptr<Command>>& get_commands() const
			{
				return commands;
			}

		private:
			HTTPServer* server = nullptr;
			const string path;

			unordered_map<string, string> info_fields;

			void* bundle;
			bool enabled = false;

			vector<unique_ptr<Handler>> handlers;
			unordered_map<http_code_t,
				unique_ptr<Handler>> override_handlers;

			unordered_map<string, string> templates;

			vector<unique_ptr<Command>> commands;

			void read_infos();
			void read_resources();

			void load();
	};

	class HTTPStream
	{
		public:
			HTTPStream() = default;
			HTTPStream(const HTTPStream&) = delete;

			inline ~HTTPStream()
			{
				if(buffer) {
					free((void*) buffer);
					buffer = nullptr;
				}
			}

			void operator=(const HTTPStream&) = delete;

			void read(const int sock);

			inline size_t data_length() const
			{
				return length;
			}

			bool check_line() const;

			bool get_line(string& buff);
			bool get_line(string& buff, const char c);

			string get();

		private:
			char* buffer = nullptr;
			size_t length = 0;

			char* i = nullptr;

			void buffer_append(const char* buff, const size_t length);
	};

	enum ParseState : uint8_t
	{
		HEADER_S = 0,
		HEAD_S = 1,
		BODY_S = 2,
		DONE_S = 3
	};

	class Response
	{
		public:
			Response(const http_code code = OK);

			inline http_code get_code() const
			{
				return code;
			}

			inline void set_code(const http_code code)
			{
				this->code = code;
			}

			inline const unordered_map<string, string>& get_fields() const
			{
				return head_fields;
			}

			inline void set_field(const string name, const string value)
			{
				head_fields[name] = value;
			}

			inline void remove_field(const string name)
			{
				head_fields.erase(name);
			}

			inline const string& get_body() const
			{
				return body;
			}

			inline void set_body(const string& body)
			{
				this->body = body;
			}

			void set_tag(const string key, const string value);

			string dump(const string& http_version, const bool body);

		private:
			http_code code = OK;
			unordered_map<string, string> head_fields;

			string body;
	};

	Response redirect(const string& to);

	inline Response redirect(const string&& to)
	{
		return redirect(to);
	}

	class error_response : public exception
	{
		public:
			inline error_response(const http_code code)
				: code{code}
			{}

			inline http_code get_code() const
			{
				return code;
			}

		private:
			const http_code code;
	};

	class Call
	{
		public:
			inline Call(HTTPServer& server, const sock_t& sock,
				const bool https)
				: server{&server}, sock{sock}, https{https}
			{}

			inline HTTPServer& get_server()
			{
				return *server;
			}

			inline const sock_t& get_socket() const
			{
				return sock;
			}

			inline bool uses_https() const
			{
				return https;
			}

			inline http_code_t get_default_code() const
			{
				return default_code;
			}

			bool handle();

			inline http_method get_method() const
			{
				return method;
			}

			inline const string& get_full_path() const
			{
				return full_path;
			}

			inline const string& get_path() const
			{
				return path;
			}

			inline const vector<string>& get_split_path() const
			{
				return split_path;
			}

			inline const unordered_map<string, string>& get_args() const
			{
				return args;
			}

			inline bool has_arg(const string& key) const
			{
				return (args.find(key) != args.cend());
			}

			inline bool has_arg(const string&& key) const
			{
				return has_arg(key);
			}

			const string& get_arg(const string& key) const;

			inline const string& get_arg(const string&& key) const
			{
				return get_arg(key);
			}

			inline const string& get_http_version() const
			{
				return http_version;
			}

			inline const unordered_map<string, string>& get_fields() const
			{
				return head_fields;
			}

			bool has_field(const string& key) const;

			inline bool has_field(const string&& key) const
			{
				return has_field(key);
			}

			const string& get_field(const string& key) const;

			inline const string& get_field(const string&& key) const
			{
				return get_field(key);
			}

			inline const string& get_body() const
			{
				return body;
			}

			void respond(Response&& response);

		private:
			HTTPServer* server;
			sock_t sock;
			bool https;

			HTTPStream stream;
			ParseState parse_state = HEADER_S;
			http_code_t default_code = OK;

			http_method method = GET;
			string http_version;

			string full_path;
			string path;
			vector<string> split_path;
			unordered_map<string, string> args;

			unordered_map<string, string> head_fields;

			string body;

			http_code_t parse_header();
			bool parse_path();
			http_code_t parse_head();
			http_code_t read_body();
	};

	class Client
	{
		public:
			Client(HTTPServer& server, const sock_t& sock, const bool https);
			~Client();

			inline HTTPServer& get_server()
			{
				return *server;
			}

			inline const sock_t& get_socket() const
			{
				return sock;
			}

			inline bool uses_https() const
			{
				return https;
			}

			bool handle();

		private:
			HTTPServer* server;
			sock_t sock;
			bool https;

			shared_ptr<Call> call;
	};

	string get_mime_type(const string& file);

	class HTTPServer
	{
		public:
			HTTPServer();
			~HTTPServer();

			void log(const string str);

			void bind_bundles();
			void bind_bundle(const string& path);

			inline vector<Bundle>& get_bundles()
			{
				return bundles;
			}

			template<typename H, typename... Args>
			inline void register_handler(Args&&... args)
			{
				handlers.emplace_back(new H(args...));
			}

			template<typename H, typename... Args>
			inline void register_override_handler(const http_code_t code,
				Args&&... args)
			{
				override_handlers.emplace(code, unique_ptr<H>(new H(args...)));
			}

			Handler* get_handler(const Call& call) const;
			Handler* get_handler(const http_code_t code) const;

			void init();

			void start_http(const port_t port);
			void stop_http();

			void start_https(const port_t port);
			void stop_https();

			void handle_call(Call& call, const http_code_t code);

			template<typename C, typename... Args>
			inline void register_command(Args&&... args)
			{
				commands.emplace_back(new C(args...));
			}

			const vector<Command*> get_commands() const;
			const Command* get_command(const string& name) const;
			bool exec_command(const string& command);

		private:
			vector<Bundle> bundles;

			int http_sock;
			int https_sock;

			vector<unique_ptr<Handler>> handlers;
			unordered_map<http_code_t,
				unique_ptr<Handler>> override_handlers;

			vector<Client> clients;
			mutex clients_mutex;

			vector<unique_ptr<Command>> commands;

			void handle_clients();
			void accept_client(const sock_t& sock, const bool https);
	};
}

#endif
