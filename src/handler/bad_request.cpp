#include "handler.hpp"
#include "../copper.hpp"

using namespace Copper;

Response BadRequestHandler::handle(HTTPServer&, Call&) const
{
	static Response response(BAD_REQUEST);
	response.set_body("<html><head><title>400 - Bad Request</title></head><body><h1>400 - Bad Request</h1><p>The request sent by the client was ill-formed!</p></body></html>");

	return response;
}
