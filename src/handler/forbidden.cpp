#include "handler.hpp"
#include "../copper.hpp"

using namespace Copper;

Response ForbiddenHandler::handle(HTTPServer&, Call&) const
{
	static Response response(FORBIDDEN);
	response.set_body("<html><head><title>403 - Forbidden</title></head><body><h1>403 - Forbidden</h1><p>You are not allowed to access this page!</p></body></html>");

	return response;
}
