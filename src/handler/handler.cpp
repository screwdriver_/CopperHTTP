#include "handler.hpp"
#include "../copper.hpp"

using namespace Copper;

static bool match(const char* str, const char* pattern)
{
	if(*pattern == '\0') {
		return (*str == '\0');
	}

	while(*pattern == '*' && *(pattern + 1) == '*') ++pattern;

	if(*str == '\0') return (*pattern == '\0'
		|| (*pattern == '*' && pattern[1] == '\0'));

	if(*pattern == '*') {
		return (match(str, pattern + 1) || match(str + 1, pattern));
	}

	if(*pattern == '\\') ++pattern;
	if(*str == *pattern) return match(str + 1, pattern + 1);
	else return false;
}

static inline bool match(const string& str, const string& pattern)
{
	return match(str.c_str(), pattern.c_str());
}

bool Handler::can_handle(const Call& call) const
{
	if(call.has_field("Host")) {
		if(!match(call.get_field("Host"), host)) return false;
	}

	return match(call.get_path(), path);
}
