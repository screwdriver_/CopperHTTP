#ifndef HANDLER_HPP
# define HANDLER_HPP

#include <stdexcept>
#include <string>

namespace Copper
{
	using namespace std;

	class HTTPServer;
	class Call;
	class Response;

	class Handler
	{
		public:
			inline Handler(const string host)
				: host{host}
			{}

			inline Handler(const string host, const string path)
				: host{host}, path{path}
			{}

			inline const string& get_host() const
			{
				return host;
			}

			inline const string& get_path() const
			{
				return path;
			}

			bool can_handle(const Call& call) const;
			virtual Response handle(HTTPServer& server, Call& call) const = 0;

		private:
			const string host;
			const string path;
	};

	inline string transform_path(string path)
	{
		if(path.empty()) throw invalid_argument("Empty resource path!");
		if(path.front() != '/') path = '/' + path;

		return path;
	}

	class ResourceHandler : public Handler
	{
		public:
			inline ResourceHandler(const string& host,
				const string& bundle_path, const string& path)
				: Handler(host, transform_path(path) + '*'),
				bundle_path{bundle_path}
			{}

			Response handle(HTTPServer& server, Call& call) const override;

		private:
			const string bundle_path;
	};

	class BadRequestHandler : public Handler
	{
		public:
			inline BadRequestHandler()
				: Handler("*")
			{}

			Response handle(HTTPServer&, Call&) const override;
	};

	class ForbiddenHandler : public Handler
	{
		public:
			inline ForbiddenHandler()
				: Handler("*")
			{}

			Response handle(HTTPServer&, Call&) const override;
	};

	class NotFoundHandler : public Handler
	{
		public:
			inline NotFoundHandler()
				: Handler("*")
			{}

			Response handle(HTTPServer&, Call&) const override;
	};

	class RHFTLHandler : public Handler
	{
		public:
			inline RHFTLHandler()
				: Handler("*")
			{}

			Response handle(HTTPServer&, Call&) const override;
	};

	class InternalServerErrorHandler : public Handler
	{
		public:
			inline InternalServerErrorHandler()
				: Handler("*")
			{}

			Response handle(HTTPServer&, Call&) const override;
	};
}

#endif
