#include "handler.hpp"
#include "../copper.hpp"

using namespace Copper;

Response InternalServerErrorHandler::handle(HTTPServer&, Call&) const
{
	static Response response(INTERNAL_SERVER_ERROR);
	response.set_body("<html><head><title>500 - Internal Server Error</title></head><body><h1>500 - Internal Server Error</h1><p>An internal server error happened!</p></body></html>");

	return response;
}
