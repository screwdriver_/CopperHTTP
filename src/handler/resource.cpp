#include "handler.hpp"
#include "../copper.hpp"

using namespace Copper;

bool is_path_safe(const string& path)
{
	size_t pos = 0;

	while((pos = path.find("/..", pos + 1)) != string::npos) {
		if(pos + 3 >= path.size()) return false;
		if(path[pos + 3] == '/' || path[pos + 3] == '\\') return false;
	}

	return true;
}

Response ResourceHandler::handle(HTTPServer&, Call& call) const
{
	const auto path = bundle_path + call.get_path();

	if(!is_path_safe(path)) throw error_response(FORBIDDEN);

	ifstream stream(path);

	if(!stream.is_open()) throw error_response(NOT_FOUND);

	try {
		stream.seekg(0, ios::end);
		const auto size = stream.tellg();
		stream.seekg(0);

		string buffer(size, '\0');
		stream.read(&buffer.front(), size);

		Response response(OK);
		response.set_field("Content-Type", get_mime_type(path));
		response.set_body(buffer);

		return response;
	} catch(const exception& e) {
		throw error_response(NOT_FOUND);
	}
}
