#include "handler.hpp"
#include "../copper.hpp"

using namespace Copper;

Response RHFTLHandler::handle(HTTPServer&, Call&) const
{
	static Response response(REQUEST_HEADER_FIELDS_TOO_LARGE);
	response.set_body("<html><head><title>431 - Request Header Fields Too Large</title></head><body><h1>431 - Request Header Fields Too Large</h1><p>The HTTP request header was too long for the server to handle it!</p></body></html>");

	return response;
}
