#include "copper.hpp"

using namespace Copper;

string Copper::get_mime_type(const string& file)
{
	const string command = string("file --mime-type ") + file;
	auto stream = popen(command.c_str(), "r");
	if(!stream) throw runtime_error("Cannot retrieve file type!");

	string buffer;
	char c[128];

	while(!feof(stream)) {
		if(!fgets(c, 128, stream)) break;
		buffer += c;
	}

	pclose(stream);
	return string(buffer.cbegin() + file.size() + 2, buffer.cend() - 1);
}

HTTPServer::HTTPServer()
{
	register_override_handler<BadRequestHandler>(BAD_REQUEST);
	register_override_handler<ForbiddenHandler>(FORBIDDEN);
	register_override_handler<NotFoundHandler>(NOT_FOUND);
	register_override_handler<RHFTLHandler>(REQUEST_HEADER_FIELDS_TOO_LARGE);
	register_override_handler
		<InternalServerErrorHandler>(INTERNAL_SERVER_ERROR);

	register_command<HelpCommand>();
	register_command<ReloadCommand>();
	register_command<BundlesCommand>();
	register_command<ExitCommand>();
}

HTTPServer::~HTTPServer()
{
	stop_http();
	stop_https();
}

void HTTPServer::log(const string str)
{
	const auto t = time(NULL);
	const auto date = ctime(&t);
	date[strlen(date) - 1] = '\0';

	cout << '[' << date << "] " << str << '\n';
	// TODO File
}

void HTTPServer::bind_bundles()
{
	log("Binding bundles...");

	errno = 0;
	mkdir(BUNDLES_DIRECTORY, 0777);

	if(errno != EEXIST) {
		errno = 0;
		return;
	}

	errno = 0;

	DIR* dir;
	dirent* entry;

	if(!(dir = opendir(BUNDLES_DIRECTORY))) {
		log("Failed to read bundles!");
		exit(-1);
	}

	while((entry = readdir(dir))) {
		const string name(entry->d_name);

		if(entry->d_type != DT_DIR) continue;
		if(name == "." || name == "..") continue;

		bind_bundle(string(BUNDLES_DIRECTORY) + name + '/');
	}

	closedir(dir);
}

void HTTPServer::bind_bundle(const string& path)
{
	try {
		bundles.emplace_back(*this, path);
		log(string("Bundle `") + path + "` bound!");
	} catch(const exception& e) {
		log(string("Failed to bind bundle `") + path + "`: " + e.what());
	}
}

Handler* HTTPServer::get_handler(const Call& call) const
{
	for(const auto& b : bundles) {
		const auto h = b.get_handler(call);
		if(h) return h;
	}

	for(const auto& h : handlers) {
		if(h->can_handle(call)) {
			return h.get();
		}
	}

	return nullptr;
}

Handler* HTTPServer::get_handler(const http_code_t code) const
{
	for(const auto& b : bundles) {
		const auto h = b.get_handler(code);
		if(h) return h;
	}

	const auto handler = override_handlers.find(code);
	if(handler != override_handlers.cend()) return handler->second.get();

	return nullptr;
}

void HTTPServer::init()
{
	log("Initializing webserver...");

	mkdir(TMP_DIRECTORY, 0777);

	thread t([this]()
		{
			while(true) {
				handle_clients();
				this_thread::sleep_for(1ms);
			}
		});

	t.detach();
}

void HTTPServer::start_http(const port_t port)
{
	log("Initializing HTTP listener...");

	try {
		http_sock = sock_create(false);
		sock_listen(http_sock, port);
	} catch(const socket_exception& e) {
		log(e.what());
		stop_http();
		exit(-1);
	}

	thread listen_thread([this]()
		{
			try {
				while(true) {
					accept_client(sock_accept(http_sock), false);
				}
			} catch(const socket_exception& e) {
				log(e.what());
				stop_http();
				exit(-1);
			}
		});

	listen_thread.detach();
}

void HTTPServer::stop_http()
{
	lock_guard<mutex> lock(clients_mutex);

	log("Stopping HTTP listener...");
	clients.clear();
	sock_close(http_sock);
}

void HTTPServer::start_https(const port_t port)
{
	lock_guard<mutex> lock(clients_mutex);

	log("Initializing HTTPS listener...");

	(void) port;
	// TODO
}

void HTTPServer::stop_https()
{
	log("Stopping HTTPS listener...");
	// TODO
}

void HTTPServer::handle_clients()
{
	lock_guard<mutex> lock(clients_mutex);
	auto c = clients.begin();

	while(c != clients.end()) {
		c = (c->handle() ? c + 1 : clients.erase(c));
	}
}

void HTTPServer::accept_client(const sock_t& socket, const bool https)
{
	lock_guard<mutex> lock(clients_mutex);
	clients.emplace_back(*this, socket, https);
}

void HTTPServer::handle_call(Call& call, const http_code_t code)
{
	auto handler = get_handler(code);
	if(!handler) handler = get_handler(call);
	if(!handler) handler = get_handler(NOT_FOUND);
	if(!handler) handler = get_handler(INTERNAL_SERVER_ERROR);

	try {
		call.respond(handler->handle(*this, call));
	} catch(const error_response& e) {
		handle_call(call, e.get_code());
	} catch(const exception& e) {
		get_handler(INTERNAL_SERVER_ERROR)->handle(*this, call);
		log(string("Internal server error: ") + e.what());
	}
}

const vector<Command*> HTTPServer::get_commands() const
{
	vector<Command*> result;

	for(const auto& c : commands) {
		result.push_back(c.get());
	}

	for(const auto& b : bundles) {
		for(const auto& c : b.get_commands()) {
			result.push_back(c.get());
		}
	}

	return result;
}

const Command* HTTPServer::get_command(const string& name) const
{
	for(const auto& c : commands) {
		if(c->get_name() == name) {
			return c.get();
		}
	}

	for(const auto& b : bundles) {
		for(const auto& c : b.get_commands()) {
			if(c->get_name() == name) {
				return c.get();
			}
		}
	}

	return nullptr;
}

bool HTTPServer::exec_command(const string& command)
{
	stringstream ss(command);
	string buffer;

	if(!getline(ss, buffer, ' ')) return false;

	const auto c = get_command(buffer);
	if(!c) return false;

	vector<string> args;

	while(getline(ss, buffer, ' ')) {
		args.push_back(buffer);
	}

	c->perform(*this, args);

	return true;
}
