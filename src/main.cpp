#include "copper.hpp"

using namespace std;

int main()
{
	using namespace Copper;

	HTTPServer server;
	server.log(string("Starting CopperHTTP version ") + VERSION + "...");
	server.bind_bundles();
	server.init();
	server.start_http(HTTP_DEFAULT);
	server.start_https(HTTPS_DEFAULT);
	server.log("Done!");

	while(true) {
		string command;
		getline(cin, command);

		if(!server.exec_command(command)) {
			cout << "Unknown command. Type \"help\" to show some help." << '\n';
		}
	}

	return 0;
}
